import './App.css';
import Header from './Components/Header';
import Form from './Components/Form';
import TodoList from './Components/TodoList';
import React,{useState} from "react";
import DoneTask from './Components/DoneTask';

function App() {
  const [input,setInput]=useState("");
  const [todos,setTodos]=useState([]);
  const [done,setDone]=useState([]);
  return (
    <div className="container">
      <div className='app'>
        <Header />
        <Form input={input} setInput={setInput} todos={todos} setTodos={setTodos}/>
        <TodoList todos={todos} setTodos={setTodos} done={done} setDone={setDone}/>
        <DoneTask todos={todos} setTodos={setTodos} done={done} setDone={setDone}/>
        </div>
    </div>
  );
}

export default App;
