import React from 'react'
import { Tooltip } from "@mui/material"

function DoneTask({todos,setTodos,done,setDone}) {
  const handleNewDelete=({id})=>{
    setTodos(todos.filter((todo)=> todo.id!==id));
    setDone(done.filter((taskDone)=> taskDone.id!==id));
}
  return (
    <div>
      {done.length>0 && <h1>Done Tasks ({done.length})</h1>}
      {done?.length>0 && done.map((item)=>(
        <li className="list-item" key={item.id}>
        <Tooltip title={<span style={{ fontSize: "20px", paddingRight:"10px",paddingLeft:"10px"}}>{item.title}</span>} placement="bottom" arrow><p className="list done" onChange={(e)=> e.preventDefault()}>{item.title}</p></Tooltip>
        <button className="button-delete task-button" onClick={()=>handleNewDelete(item)}>
            <i className="fa fa-trash" aria-hidden="true"></i>
        </button>
        </li>
      ))}
    </div>
  )
}

export default DoneTask