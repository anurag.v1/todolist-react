import React from 'react';
import {v4} from 'uuid';

function Form({input,setInput,todos,setTodos}) {
    const onInputChange=(e)=>{
        setInput(e.target.value);
    };
    const onFormSubmit=(e)=>{
        e.preventDefault();
        if(input!==""){
        setTodos([...todos,{id:v4(),title: input,completed: false}]);
        }
        setInput("");
    };
  return (
    <form onSubmit={onFormSubmit}>
        <input autoFocus="autofocus" type="text" placeholder='Enter your todoitem' className='task' value={input} onChange={onInputChange}/>
        <button className="add" type="submit">Add</button>
    </form>
  )
}

export default Form