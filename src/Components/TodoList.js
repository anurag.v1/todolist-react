import React from 'react'
import { Tooltip } from "@mui/material"

function TodoList({todos,setTodos,done,setDone}) {
    const handleComplete=(todo)=>{
        setDone([...done, todo])
        handleDelete(todo)
    }
    const handleDelete=({id})=>{
        setTodos(todos.filter((todo)=> todo.id!==id));
    }
  return (
    <div>
        {todos?.length>0 && <h1>Pending Tasks ({todos.length})</h1>}
        {todos.map((todo)=>(
            <li className="list-item" key={todo.id}>
            <Tooltip title={<span style={{ fontSize: "20px", paddingRight:"10px",paddingLeft:"10px"}}>{todo.title}</span>} placement="bottom" arrow><p className="list notDone" onChange={(e)=> e.preventDefault()}>{todo.title}</p></Tooltip>
            <button className="button-complete task-button" onClick={()=>handleComplete(todo)}>
                <i className="fa fa-check" aria-hidden="true"></i>
            </button>
            <button className="button-delete task-button" onClick={()=>handleDelete(todo)}>
                <i className="fa fa-trash" aria-hidden="true"></i>
            </button>
            </li>
        ))}
        
    </div>
  )
}

export default TodoList